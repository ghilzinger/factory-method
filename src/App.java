public class App {
    private static Dialogo dialogo;

    public static void main(String[] args) throws Exception {
        configurar();
        correrLogica();
    }

    static void configurar() {
        if (System.getProperty("os.name").equals("Windows 10")) {
            dialogo = new DialogoWindows();
        } else {
            dialogo = new DialogoHTML();
        }
    }

    static void correrLogica() {
        dialogo.renderizarVentana();
    }
}
