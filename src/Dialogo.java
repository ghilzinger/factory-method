public abstract class Dialogo {
    public void renderizarVentana() {
        // código

        Boton botonOK = crearBoton();
        botonOK.render();
    }

    public abstract Boton crearBoton();
}
